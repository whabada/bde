package vicra;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class AverageColorReducer extends Reducer<IntWritable, Text, IntWritable, Text> {

	/**
	 * 
	 * Hier wird gezaehlt, wie häufig ein R G B Code vom Mapper "gesehen" wurde
	 * Dieses Ergebnis wird aufsummiert und dann als Durchschnittswert ausgegeben 
	 */
	@Override
	protected void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

		/**
		 * 	TODO Ben
			//TODO Der Mapper soll den Durchschnittswert des Pixel weitergeben, 
			int num = w * h;
			float [] resultArray = new float [3];
			resultArray[0]= sumr / num;
			resultArray[1]= sumg / num;
			resultArray[2]= sumb / num;

		 */
		long sumPixel =0;
		long sumr = 0;
		long sumg = 0;
		long sumb = 0;

		for (Text val : values){

			// die RGB Werte werden durch Split wieder auseinander gezogen
			// Bei der Schleife werden die Summe aller Bilder mitgezaehlt.
			// Die RGB werte werden durch die Anzahl der Bilder geteilt und als Text ausgegeben
			// im Text sind die Werte wie folgt angeordnet: sumr + " " +  sumg + " " + sumb +" " + sumPixel;

			String [] result = val.toString().split("\t");
			sumPixel = Long.parseLong(result[3]);
			sumr += Long.parseLong(result[0]) / sumPixel;
			sumg += Long.parseLong(result[1]) / sumPixel;
			sumb += Long.parseLong(result[2]) / sumPixel;					
		}

		long [] resultArray = new long [3];
		resultArray[0]= sumr;
		resultArray[1]= sumg;
		resultArray[2]= sumb;

		String stringResult = resultArray [0]+ " " + resultArray [1] + resultArray [2];
		Text result = new Text(stringResult);
		context.write(key, result);
	}

}