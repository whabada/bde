package vicra;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class ColorAnalyzerMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
	//TODO Input Inputstream aus HDFS File

	private final static IntWritable one = new IntWritable(1);
	private Text rgb = new Text();

	/**
	 * Die Mapper Funktion erhaelt ein Bild und analysiert die Farbe saemtlicher Pixel des erhaltenen Images. 
	 * Die Funktion funktioniert nur, wenn das Bild nicht null ist und groeßer als 1x1 ist.
	 * Für jede Farbe R, G, B wird ausgebenen "RGB Farbcode, 1". 
	 * @param key Nummer des Frames 
	 * @param BufferedImage image
	 * @param context
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void map(IntWritable key, BufferedImage image, Context context) throws IOException, InterruptedException {
		// Verify that image was properly decoded and is of sufficient size)

		if (image != null && image.getWidth() > 1 && image.getHeight() > 1) {
			// Get dimensions of image
			int w = image.getWidth();
			int h = image.getHeight();
			int x1 = w;
			int y1 = h;
			long sumPixel = w*h;
			long sumr = 0, sumg = 0, sumb = 0;
			for (int x = 0; x < x1; x++) {
				for (int y = 0; y < y1; y++) {
					Color pixel = new Color(image.getRGB(x, y));
					sumr += pixel.getRed(); //rot
					sumg += pixel.getGreen(); // gruen
					sumb += pixel.getBlue(); // blau
				}
			}

			String result = sumr + " " +  sumg + " " + sumb +" " + sumPixel;
			this.rgb.set(result);
			//TODO R G B sind die Key Words fur den Wert, Reducer group by 
			/** Bsp aus link WordCountsInDocuments.java
			 *  String[] wordAndDocCounter = value.toString().split("\t");
      			String[] wordAndDoc = wordAndDocCounter[0].split("@");
      			this.docName.set(wordAndDoc[1]);
      			this.wordAndCount.set(wordAndDoc[0] + "=" + wordAndDocCounter[1]);
      			context.write(this.docName, this.wordAndCount);
			 */

			context.write(rgb, one);
		}
		else { 
			//Kein Bild geladen
			System.out.println("kein Bild.");
		}
	}
}
