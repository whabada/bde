package vicra;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		// Initialize and configure MapReduce job
	    Job job = Job.getInstance();
	    // Set input format class which parses the input HIB and spawns map tasks
	    job.setInputFormatClass(SequenceFileInputFormat.class);
	    // Set the driver, mapper, and reducer classes which express the computation
	    job.setJarByClass(Main.class);
	    job.setMapperClass(ColorAnalyzerMapper.class);
	    job.setReducerClass(AverageColorReducer.class);
	    // Set the types for the key/value pairs passed to/from map and reduce layers
	    job.setMapOutputKeyClass(IntWritable.class);
	    job.setMapOutputValueClass(IntWritable.class);
	    job.setOutputKeyClass(IntWritable.class);
	    job.setOutputValueClass(Text.class);
	    
		FileInputFormat.addInputPath(job, new Path(args[0])); 
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
